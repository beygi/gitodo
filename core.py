#!/usr/bin/python
#-*- coding:utf-8 -*-
import subprocess
import os
import re
import cPickle as pickle

from prettytable import PrettyTable

class GitodoCore:
    def __init__(self):
        self.dbname = os.getcwd() + "/.git/db.p"

    def todoList(self):
        try:
            todos = pickle.load(open(self.dbname, "r"))
        except:
            todos = {}

        pt = PrettyTable(["#","location","reference id","message"])
        pt.align["location"] = "l"
        i = 1
        for file_name in todos.keys():
            for message in todos[file_name].keys():
                pt.add_row([i, file_name, todos[file_name][message]['ref_id'], message])
                i+=1
        print pt

    def scanFiles(self, dir=os.getcwd()):
        todos = {}
        new_scan = {}
        try:
            old_scan = pickle.load(open(self.dbname, "r"))
        except:
            old_scan = {}

        # if db exists do not run a full scan
        changed_only = True if os.path.exists(self.dbname) else False
        # now scan the shit out of my files
        for file_name in self.getTrackedFiles(changed_only=changed_only):
            new_scan[file_name] = self.extractTodoItems(file_name)

        # rebuild the stupid todo list

        # if file didn't change in new scan do not make it that complicated
        for file_name in old_scan.keys():
            if not file_name in new_scan.keys():
                todos[file_name] = old_scan[file_name]
                del old_scan[file_name]
                continue

        for file_name in new_scan.keys():
            # if new file, it doesn't exist in old scans
            if not file_name in old_scan.keys():
                if new_scan[file_name]:
                    todos[file_name] = new_scan[file_name]
                del new_scan[file_name]
                continue

            todos[file_name] = {}
            for task_name in new_scan[file_name].keys():
                todos[file_name][task_name] = new_scan[file_name][task_name]
                del new_scan[file_name][task_name]
                if task_name in old_scan[file_name].keys():
                    todos[file_name][task_name]['ref_id'] = old_scan[file_name][task_name]['ref_id']
                    del old_scan[file_name][task_name]
            if todos[file_name] == {}:
                del todos[file_name]

        removed_todos = old_scan

        pickle.dump(todos, open(self.dbname, "w" ))

    def enable(self, dir=os.getcwd()):
        if(os.path.isdir(dir+"/.git")):
            print "trying to enable gitodo for your repository ... "
            #add hook to the repository
            if(not os.path.exists(dir+"/.git/hooks/post-commit")):
                #create post-commit hook
                with open(dir+"/.git/hooks/post-commit", "w") as f:
                    f.write("#!/bin/sh"+"\n")
                    f.write("gitodo --scan")
                print "Done"
                os.chmod(dir+"/.git/hooks/post-commit", 0777)
            else:
                #append to post-commit hook
                with open(dir+"/.git/hooks/post-commit", "a") as f:
                    f.write("gitodo --scan")
            self.scanFiles()

    def disable(self, dir=os.getcwd()):
        print "disable gtodo for this repo"
        hookIn= open(dir+"/.git/hooks/post-commit")
        hookOut= open(dir+"/.git/hooks/post-commit",'w')
        line=""
        for line in hookIn:
           line = line.replace("gitodo scan", "")
        hookOut.write(line)
        hookIn.close()
        hookOut.close()
        #remove db.p
        os.remove(dir+"/.git/db.p")


    def checkGitodo(self, dir=os.getcwd()):
        if(not os.path.isdir(dir+"/.git")):
            print "this isn't a git repository"
            return False
        if(not os.path.exists(dir+"/.git/hooks/post-commit") or not('gitodo' in open(dir+"/.git/hooks/post-commit").read())):
            print "gitodo is not enabled in this repository"
            return False
        return True

    def extractTodoItems(self, file_name):
        todos = {}

        with open(file_name, 'r') as file_obj:
            lines = file_obj.readlines()
            for i in range(len(lines)):
                result = re.findall('^\s*(\#|//|--)\s*@?todo\s?:\s*(.*)$', lines[i], flags=re.IGNORECASE)
                if result:
                    result = result[0]
                    todos[result[1]] = {
                        'line_number': i+1,
                        'ref_id': '',
                    }

        return todos

    def getTrackedFiles(self, changed_only):
        if changed_only:
            #TODO: git command doesn't work as expected
            cmd = 'git show --pretty="format:"  --name-only'
        else:
            cmd = 'git ls-tree --name-only -r HEAD'

        try:
            results = subprocess.check_output(cmd, shell=True, stderr=subprocess.STDOUT)
        except subprocess.CalledProcessError as err:
            if err.returncode == 128 and 'fatal: Not a valid object name HEAD' in err.output:
                # empty repo do noting
                return []
            else:
                print '"',err.output,'"'
                raise err
        files = [file_name for file_name in results.splitlines() if file_name and os.path.exists(file_name)]
        return files
