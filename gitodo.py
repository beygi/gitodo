#!/usr/bin/python
#-*- coding:utf-8 -*-

#Python import
import os
from sys import argv
from optparse import OptionParser

#project import
import core
from plugins.bitbucket import addBit, removeBit
from plugins.github import addGitHub, removeGitHub

def main():
    parser = OptionParser(usage='%prog args')
    parser.add_option('-l', '--list', action="store_true", dest='list', help='list todos')
    parser.add_option('-s', '--scan', action="store_true", dest='scan', help='scan todos')
    parser.add_option('-e', '--enable', action="store_true", dest='enable', help='enable gitodo')
    parser.add_option('-d', '--disable', action="store_true", dest='disable', help='disable gitodo')

    gittodo_core = core.GitodoCore()

    opts, args = parser.parse_args()

    if opts.list:
            gittodo_core.todoList()
    elif opts.scan:
            if(gittodo_core.checkGitodo()):
                gittodo_core.scanFiles()
            else:
                gittodo_core.enable()
    elif opts.enable:
            if(not gittodo_core.checkGitodo()):
                gittodo_core.enable()
            else:
                print "gitodo already enabled for yor repository"
    elif opts.disable:
            if(gittodo_core.checkGitodo()):
                gittodo_core.disable()
    else:
        parser.print_help()
    # elif(arg == 'add' and argv[2] == 'bitbucket'):
    #         if(checkGitodo()):
    #             addBit()

    # elif(arg == 'remove' and argv[2] == 'bitbucket'):
    #         removeBit()
    # elif(arg == 'add' and argv[2] == 'github'):
    #         addGitHub()
    # elif(arg == 'remove' and argv[2] == 'github'):
    #         removeGitHub()
    # else:
    #     usage()
    # except:
    #     usage()

if __name__ == "__main__":
    main()
